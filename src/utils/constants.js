export const ENV = {
    API_URL: "https://lizard-server.vercel.app",
    ENDPOINTS:{
        LOGIN:'api/auth/signin',
        REGISTER:'api/auth/signup',
        USER:'api/users',
    },
    STORAGE: {
        TOKEN: "token",
    }
}